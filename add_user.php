<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 19.10.17
 * Time: 10:29
 */
require ("Core.php");
require ("Users.php");
require ("Accounts.php");

$host = 'localhost';
$user = 'root';
$password = '123';
$table = 'Test';

$mysql = new mysqli($host, $user, $password, $table);
Core::setup($mysql);


if ($post = $_POST['field'])
{
    $email = $post['email'];
    $name = trim($post['name']);
    $address = $post['address'];

    if (!$email)
    {
        echo 'Не указан email!';
        exit;
    }

    if (!$name)
    {
        echo 'Не указано имя!';
        exit;
    }

    if (!$address)
    {
        echo 'Не указан адрес!';
        exit;
    }

    $user = new Users();
    $user->usr_name = $name;
    $user->usr_email = $email;
    $user->usr_address = $address;
    $result = $user->Add();

    $acc = Users::Acounts($result);
    echo json_encode(['id' => $result, 'object' => $user, 'acc' => $acc]);
}
