<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 19.10.17
 * Time: 10:29
 */
require ("Core.php");
require ("Users.php");
require ("Accounts.php");

$host = 'localhost';
$user = 'root';
$password = '123';
$table = 'Test';

$mysql = new mysqli($host, $user, $password, $table);
Core::setup($mysql);


if ($post = $_POST['field'])
{
    $account = $post['account'];
    $user_id = trim($post['user']);

    if (!$account)
    {
        echo 'Не указан акк!';
        exit;
    }

    if (!$user_id)
    {
        echo 'Не указан пользователь!';
        exit;
    }

    $acc_new = new Accounts();
    $acc_new->account = $account;
    $acc_new->user_id = $user_id;
    $acc_new->added = date("Y-m-d H:i:s");
    $result = $acc_new->Add();

    $acc = Users::Acounts($user_id);
    echo json_encode(['id' => $user_id, 'date' => $acc]);
}
