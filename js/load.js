/**
 * Created by artem on 20.10.17.
 */
var total = 0;

$(document).ready(function() {
    $('#user_form').submit(function(){
        $.post("add_user.php", $("#user_form").serialize(),  function(response) {
            request = JSON.parse(response);
            id = request.id;
            object = request.object;
            acc = request.acc;
            $('<tr>')
                .attr('id','tr_'+id)
                .append (
                    $('<td>')
                        .attr('class','name')
                        .append(
                            $('<p>'+object.usr_name+'</p>')
                        )

                )
                .append (
                    $('<td>')
                        .attr('class','email')
                        .append(
                            $('<p>'+object.usr_email+'</p>')
                        )

                )
                .append (
                    $('<td>')
                        .attr('class','address')
                        .append(
                            $('<p>'+object.usr_address+'</p>')
                        )

                )
                .append (
                    $('<td>')
                        .attr('class','count')
                        .append(
                            $('<p>'+acc.count+'</p>')
                        )

                )
                .append (
                    $('<td>')
                        .attr('class','date')
                        .append(
                            $('<p>'+acc.added+'</p>')
                        )

                )
                .appendTo('#table');
            $('.select')
                .append($("<option></option>")
                    .attr("value",id)
                    .text(object.usr_name));
            $('#user_form_success').html('Добавлено!');
            $('#user_form')[0].reset();
        });
        return false;
    });

    $('#account_form').submit(function(){
        $.post("add_account.php", $("#account_form").serialize(),  function(response) {
            request = JSON.parse(response);
            id = request.id;
            date = request.date.added;
            count = request.date.count;
            $('#tr_'+id+' .count').text(count);
            $('#tr_'+id+' .date').text(date);
            $('#account_form_success').html('Добавлено!');
            $('#account_form')[0].reset();
        });
        return false;
    });
});
