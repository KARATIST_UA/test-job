<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 19.10.17
 * Time: 20:49
 */

require ("Core.php");
require ("Users.php");
require ("Accounts.php");

$host = 'localhost';
$user = 'root';
$password = '123';
$table = 'Test';

$mysql = new mysqli($host, $user, $password, $table);
Core::setup($mysql);

$users = Users::findAll();

?>


<html>
    <head>
        <meta charset="utf-8">

        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <div class="container">
            <div class="table">
                <table id="table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Accounts</th>
                        <th>Date added</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($users as $user): ?>
                        <tr id="tr_<?= $user->id; ?>">
                            <td class="name"><?= $user->usr_name; ?></td>
                            <td class="email"><?= $user->usr_email; ?></td>
                            <td class="address"><?= $user->usr_address; ?></td>
                            <?php $acc = Users::Acounts($user->id); ?>
                            <td class="count"><?= $acc->count ?></td>
                            <td class="date"><?= $acc->added ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div class="flex_box">
                <div class="in_flex_50">
                    <form method="post" id="user_form" action="add_user.php">
                        <p><label>Имя <input type="text" name="field[name]" required></label></p>
                        <p><label>Email <input type="email" name="field[email]" required></label></p>
                        <p><label>Адрес <input type="text" name="field[address]" required></label></p>
                        <p><button type="submit">Добавить</button></p>
                    </form>
                    <div id="user_form_success"></div>
                </div>
                <div class="in_flex_50">
                    <form method="post" id="account_form" action="add_account.php">
                        <p><label>Номер <input type="text" name="field[account]" required></label></p>
                        <p><label>Email
                                <select name="field[user]" class="select">
                                    <option selected disabled>Выберите пользователя</option>
                                    <?php foreach( $users as $user ): ?>
                                        <option value="<?= $user->id; ?>"><?= $user->usr_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </label></p>
                        <p><button type="submit">Добавить</button></p>
                    </form>
                    <div id="account_form_success"></div>
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="/js/load.js"></script>
    </body>
</html>




