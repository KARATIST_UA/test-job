<?php

abstract class Core{

    public static $db;	//Объект базы данных
    public static $stack;	//Стэк запросов

    static public function setup(mysqli $dbi,$enc = "utf8")
    {
        if (is_object($dbi))
        {
            self::$db = $dbi;
            self::$stack = new SplStack();
            return self::setEncoding($enc);
        }else
            {
            throw new Exception("Параметр $dbi не является объектом mysqli", 1);
            return false;
        }
    }

    static function setEncoding($enc)
    {
        $result = self::$db->query("SET NAMES '$enc'");
        self::$stack->push("SET NAMES '$enc' [".($result ? "TRUE" : self::getError())."]");
        return $result ? true : false;
    }

    static function getError()
    {
        return self::$db->error." [".self::$db->errno."]";
    }

    private static function escape($string)
    {
        return mysqli_real_escape_string(self::$db,$string);
    }

    private static function _getVars()
    {
        return array_filter(get_class_vars(get_called_class()),function($elem){
            if (!is_object($elem)) return true;
        });
    }

    static function findID($id)
    {
        if (is_numeric($id))
        {
            $query = "SELECT * FROM `".get_called_class()."` WHERE `".key(self::_getVars())."` = $id LIMIT 1";
            $result = self::$db->query($query);
            self::$stack->push($query." [".$result->num_rows."]");
            if ($result->num_rows == 1)
            {
                $row = $result->fetch_object();
                $cName = get_called_class();
                $rClass = new $cName();
                foreach ($row as $key => $value) $rClass->$key = $value;
                return $rClass;
            }
            else return false;
        }
        else return false;
    }

    static function Acounts($id)
    {
        $query = "SELECT COUNT(*) as count, added FROM `Accounts` WHERE user_id = ".$id."";
        $result = self::$db->query($query);
        self::$stack->push($query." [".$result->num_rows."]");
        if ($result->num_rows == 1)
        {
            $row = $result->fetch_object();
            $rClass = new Accounts();
            foreach ($row as $key => $value)
            {
                $rClass->$key = $value;
            }
            return $rClass;
        }
    }

    static function findAll()
    {
        $query = "SELECT * FROM `".get_called_class()."` ";
        $result = self::$db->query($query);
        self::$stack->push($query." [".$result->num_rows."]");
        if ($result->num_rows == 1)
        {
            $row = $result->fetch_object();
            $cName = get_called_class();
            $rClass = new $cName();
            foreach ($row as $key => $value)
            {
                $rClass->$key = $value;
            }
            return $rClass;
        }
        else{
            $result_array = [];
            while( $row = $result->fetch_object() ) {
                $cName = get_called_class();
                $rClass = new $cName();
                foreach ($row as $key => $value)
                {
                    $rClass->$key = $value;
                }
                $result_array[] = $rClass;
            }
            return $result_array;
        }
    }

    public function Save()
    {
        $id = key(self::_getVars());
        if (!isset($this->$id) || empty($this->$id)) return $this->Add();
        $query = "UPDATE `".get_called_class()."` SET ";
        $columns = self::_getVars();
        $Update = array();
        foreach ($columns as $k => $v)
        {
            if ($id != $k)
            {
                $Update[] = "`".$k."` = ".self::RenderField($this->$k);
            }
        }
        $query .= join(", ",$Update);
        $query .= " WHERE `$id` = ".self::escape($this->$id)." LIMIT 1";
        $result = self::$db->query($query);
        self::$stack->push($query." [".($result ? "TRUE" : self::getError())."]");
        return ($result) ? true : false;
    }

    private static function RenderField($field){
        $r = "";
        switch (gettype($field)) {
            case "integer":
                case "float":
                    $r = $field;
            break;
            case "NULL":
                $r = "NULL";  break;
            case "boolean":
                $r = ($field) ? "true" : "false"; break;
            case "string":
                $p_function = "/^[a-zA-Z_]+\((.)*\)/";
                preg_match($p_function, $field,$mathes);
                if (isset($mathes[0]))
                {
                    $p_value = "/\((.+)\)/";
                    preg_match($p_value, $field,$mValue);
                    if (isset($mValue[0]) && !empty($mValue[0]))
                    {
                        $pv = trim($mValue[0],"()");
                        $pv = "'".self::escape($pv)."'";
                        $r = preg_replace($p_value, "($pv)" , $field);
                    }
                    else $r = $field;
                }
                else $r = "'".self::escape($field)."'";
                break;
            default:
                $r = "'".self::escape($field)."'";
                break;
        }
        return $r;
    }

    public function Add()
    {
        $query = "INSERT INTO `".get_called_class()."` (";
        $columns = self::_getVars();
        $q_column = array();
        $q_data = array();
        foreach ($columns as $k => $v)
        {
            $q_column[] = "`".$k."`";
            $q_data[] 	= self::RenderField($this->$k);
        }
        $query .= join(", ",$q_column).") VALUES (";
        $query .= join(", ",$q_data).")";
        $result = self::$db->query($query);
        $insert_id = self::$db->insert_id;
        self::$stack->push($query." [".($result ? $insert_id : self::getError())."]");
        return ($result) ? $insert_id : false;
    }
}